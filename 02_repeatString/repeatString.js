const repeatString = function(String, num) {
    let resultado = '';
     for (let i = 0; i < num; i++){
         resultado += String;
         }
    if(num < 0){
        return 'ERROR'
     }
     return resultado;
};

// Do not edit below this line
module.exports = repeatString;
console.log(repeatString('hey',3));
console.log(repeatString('hey',10));
console.log(repeatString('hey',1));
console.log(repeatString('hey',0));
console.log(repeatString('hey',-3));

