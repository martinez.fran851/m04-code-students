# Ejercicio 02 - repeatString

Escribe una función que simplemente repita la cadena un número dado de veces:

```javascript
repeatString('hey', 3) // devuelve 'heyheyhey'
```

Esta función tomará dos argumentos, `string` y `num`.

*Nota:* Los ejercicios después de este no tendrán argumentos proporcionados como en este - deberás proporcionarlos tú mismo a partir de ahora. Así que lee cuidadosamente el README de cada ejercicio para ver qué tipo de argumentos se esperarán.

Notarás en este ejercicio que hay múltiples pruebas (ver en el archivo `repeatString.spec.js`). Solo la primera prueba está habilitada actualmente. Así que, después de asegurarte de que esta primera pasa, habilita las demás una por una eliminando el `.skip` de la función `test.skip()`.

## Sugerencias

- Presta atención a la llamada de función anterior: ¿cómo se está llamando exactamente?

- Vas a querer usar un bucle para este ejercicio.

- Crea una variable para almacenar la cadena que vas a devolver, crea un bucle que se repita el número de veces dado y agrega la cadena dada al resultado en cada iteración.

- Si al ejecutar `npm test repeatString.spec.js` obtienes resultados similares a los siguientes, asegúrate de haber habilitado el resto de las pruebas, como se describe en las instrucciones anteriores.

```
Test Suites: 1 passed, 1 total
Tests: 6 skipped, 1 passed, 7 total
```