# Ejercicio 08 - Calculadora

El objetivo de este ejercicio es crear una calculadora que realice las siguientes operaciones:

sumar, restar, obtener la suma, multiplicar, obtener la potencia y encontrar el factorial

Para lograr esto, completa cada función con tu solución. Asegúrate de devolver el valor para que puedas probarlo en Jest. Para ver el valor esperado, consulta el archivo de especificaciones que contiene los casos de prueba Jest.

