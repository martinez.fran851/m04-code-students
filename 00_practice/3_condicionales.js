// Funciones
function identificarNumero(num1) {
    if (num1 > 0)
        return 'El número es positivo';
    if (num1 < 0)
        return 'El número es negativo';
    if (num1 === 0)
        return 'El número es cero';
}



// Ejemplos de uso
console.log('Positivo ' + identificarNumero(5));
console.log('Negativo '+ identificarNumero(-3));
console.log('Cero '+ identificarNumero(0));

// Do not edit below this line
module.exports = { identificarNumero };
