# Ejercicio 00 - ejercicios iniciales

## 1 Variables

Declara tres variables: numero (un número entero), texto (una cadena de texto) y esVerdadero (un valor booleano). Asigna valores a cada variable y muestra sus tipos utilizando la función typeof. Luego, realiza una concatenación de la cadena de texto con el número y muestra el resultado.

Las variables son globales y se deben declarar fuera de la función ``variables``.

## 2 Operadores

**IMPORTANTE** Para lanzar los tests de este ejercicio se debe modiciar el ``.skip`` de los tests!

Crea una función para cada operación que acepte dos números como parámetros. Dentro de las funciónes, realiza las siguientes operaciones:

- Suma los dos números.
- Resta el segundo número del primero.
- Multiplica los dos números.
- Divide el primero por el segundo.

Las funciones deben llamarse:

    sumar, restar, multiplicar, dividir

## 3 Condicionales

**IMPORTANTE** Para lanzar los tests de este ejercicio se debe modiciar el ``.skip`` de los tests!

1) Identificar Número (identificarNumero): Implementa una función llamada identificarNumero que tome un número como parámetro y devuelva una cadena de texto indicando si el número es positivo, negativo o cero.


```JavasCript
 // Ejemplo de uso
console.log(identificarNumero(5)); // Debería imprimir "El número es positivo"
console.log(identificarNumero(-3)); // Debería imprimir "El número es negativo"
console.log(identificarNumero(0)); // Debería imprimir "El número es cero"
```

## 4 Bucles

**IMPORTANTE** Para lanzar los tests de este ejercicio se debe modiciar el ``.skip`` de los tests!

1) Palíndromo (esPalindromo): Implementa una función llamada esPalindromo que tome una palabra como parámetro y devuelva true si la palabra es un palíndromo y false si no lo es.


```JavaScript
// Ejemplo de uso
console.log(esPalindromo("reconocer")); // Debería imprimir true
console.log(esPalindromo("javascript")); // Debería imprimir false
```

**NOTA** Para trabajar con arrays desde strings utilizamos el método ``.split(caracter)`` donde caracter es el string por el que queremos cortar el array. En caso de querer todas las letras separadas, caracter = ``''``.

**IMPORTANTE** Este ejercicio se debe resolver con bucles.

2) Suma de Números Pares (sumaNumerosPares): Implementa una función llamada sumaNumerosPares que tome un número como parámetro y calcule la suma de todos los números pares desde 1 hasta el número dado.

```JavasCript
// Ejemplo de uso
console.log(sumaNumerosPares(8)); // Debería imprimir 20 (2 + 4 + 6 + 8)
console.log(sumaNumerosPares(10)); // Debería imprimir 30 (2 + 4 + 6 + 8 + 10)
```

**IMPORTANTE** Este ejercicio se debe resolver con bucles.

## 5 Arrays

1) Máximo en un Array (encontrarMaximo): Implementa una función llamada encontrarMaximo que tome un array de números como parámetro y devuelva el número máximo presente en el array.

```JavasCript
// Ejemplo de uso
console.log(encontrarMaximo([3, 7, 1, 10, 5])); // Debería imprimir 10
console.log(encontrarMaximo([-2, -8, -1, -5])); // Debería imprimir -1
```



2) Invertir un Array (invertirArray): Implementa una función llamada invertirArray que tome un array como parámetro y devuelva un nuevo array con los elementos en orden inverso.


```JavasCript
// Ejemplo de uso
console.log(invertirArray([1, 2, 3, 4])); // Debería imprimir [4, 3, 2, 1]
console.log(invertirArray(['a', 'b', 'c'])); // Debería imprimir ['c', 'b', 'a']
```



3) Suma de Elementos en un Array (calcularSuma): Implementa una función llamada calcularSuma que tome un array de números como parámetro y devuelva la suma de todos los elementos del array.



```JavasCript
// Ejemplo de uso
console.log(calcularSuma([1, 2, 3, 4])); // Debería imprimir 10
console.log(calcularSuma([-5, 10, 2])); // Debería imprimir 7
```



