// Funciones
function encontrarMaximo(array) {
   if(array.length === 0){
       return undefined;
   }
   let maximo = array[0];
   for(let i = 1; i <array.length; i++){
       if (array[i>maximo]){
           maximo = array[i];
       }
   }
   return maximo;
}
function invertirArray(array) {
    function array.reverse();


}
function calcularSuma(array) {
    let suma = 0;
    for (let i = 0; i < array.length; i++){
        suma += array[i];
    }
    return suma;

}

// Ejemplo de uso
console.log(encontrarMaximo([3,7,1,10,5]));
console.log(encontrarMaximo([-2,-8,-1,-5]));

console.log(invertirArray([1,2,3,4]));
console.log(invertirArray(['a','b','c']));

console.log(calcularSuma([1,2,3,4]));
console.log(calcularSuma([-5,10,2]));


// Do not edit below this line
module.exports = { encontrarMaximo, invertirArray, calcularSuma };
