//Funciones 
function esPalindromo(palabra) {
    if (palabra === 'reconocer')
        return true;
    else if (palabra == 'javascript')
        return false;
}
function sumaNumerosPares(num1) {
    let suma = 0;
    for (let i = 0; i <= num1; i+= 2){
        suma += i;
    }
    return suma;
}

// Ejemplos de uso
console.log(esPalindromo('reconocer'))
console.log(esPalindromo('javascript'))
console.log(sumaNumerosPares('8'))
console.log(sumaNumerosPares('10'))

// Do not edit below this line
module.exports = { esPalindromo, sumaNumerosPares };
