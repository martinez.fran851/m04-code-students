const fibonacci = function(n) {
    if (n < 0) {
        return "OOPS";
    } else if (n === 0) {
        return 0;
    } else if (n === 1 || n === 2) {
        return 1
    } else {
        let a = 1;
        let b = 1;

        for (let i=3;i<=n;i++) {
            let temp = a+b;
            a = b;
            b = temp;
        }

        return b;
    }
};

// Do not edit below this line
module.exports = fibonacci;

