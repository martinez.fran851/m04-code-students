# Ejercicio 10 - Fibonacci

Crea una función que devuelva un miembro específico de la secuencia de Fibonacci:

> Una serie de números en la cual cada número (número de Fibonacci) es la suma de los dos números anteriores. El más simple es la serie 1, 1, 2, 3, 5, 8, etc.

```javascript
fibonacci(4) // returns the 4th member of the series: 3  (1, 1, 2, 3)
fibonacci(6) // returns 8
```
