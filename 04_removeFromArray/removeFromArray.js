const removeFromArray = function(array, ...numbers) {
    return array.filter(element => !numbers.includes(element))
};

console.log(removeFromArray([1, 2, 3, 4], 3));

// Do not edit below this line
module.exports = removeFromArray;
