# Ejercicio 04 - removeFromArray

Implementa una función que tome un array y otros argumentos, y luego elimine esos otros argumentos de ese array:

```javascript
removeFromArray([1, 2, 3, 4], 3); // should remove 3 and return [1,2,4]
```

## Pistas

El primer test en este ejercicio es bastante fácil, pero hay algunas cosas en las que debes pensar (o buscar) para los tests posteriores:

- Cómo eliminar un solo elemento de un array.
- Cómo manejar múltiples argumentos opcionales en una función de JavaScript.
- [Consulta este enlace](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Functions/arguments). Desplázate hacia abajo hasta la parte sobre `Array.from` o el operador de propagación. - [O este enlace](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Functions/rest_parameters).
