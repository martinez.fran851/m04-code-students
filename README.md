# Ejercicios de JavaScript

Estos ejercicios de JavaScript están destinados a complementar el contenido de JavaScript en The Odin Project (TOP). Solo deben realizarse cuando se indique durante el curso del plan de estudios.

**Nota:** El archivo `generator-exercise` en realidad no es un ejercicio; es un script que genera ejercicios. Se creó para ayudar a escribir eficientemente estos ejercicios.

## Cómo utilizar estos ejercicios

1. Haz un fork y clona este repositorio. Para aprender cómo hacer un fork, consulta la documentación de GitHub sobre cómo [hacer un fork](https://docs.github.com/en/get-started/quickstart/fork-a-repo).
   - Las copias de repositorios en tu máquina se llaman clones. Si necesitas ayuda clonando tu entorno local, puedes aprender cómo hacerlo en la documentación de GitHub sobre [clonar un repositorio](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/cloning-a-repository-from-github/cloning-a-repository).
2. Antes de comenzar a trabajar en los ejercicios, asegúrate de tener instalado lo siguiente:
   - **NPM**. Deberías haber instalado NPM en nuestra lección [Installing Node.js](https://www.theodinproject.com/lessons/foundations-installing-node-js). Si necesitas verificarlo, escribe `npm --version` en tu terminal. Si obtienes `Command 'npm' not found, but can be installed with:`, **no sigas las instrucciones en la terminal** para instalarlo con `apt-get`, ya que esto causa problemas de permisos. En su lugar, regresa a la lección de instalación e instala Node con NVM siguiendo las instrucciones allí.
   - **Jest**. Después de clonar este repositorio en tu máquina local e instalar NPM, entra al directorio recién creado (`cd javascript-exercises`) y ejecuta `npm install`. Esto instalará Jest y configurará la plataforma de pruebas basada en nuestra configuración predefinida.
3. Cada ejercicio incluye 3 archivos: un archivo markdown con una descripción de la tarea, un archivo JavaScript vacío (o casi vacío) y un conjunto de pruebas. Para completar un ejercicio, debes ir al directorio del ejercicio con `cd exerciseName` en la terminal y ejecutar `npm test exerciseName.spec.js`. Esto debería ejecutar el archivo de pruebas y mostrarte la salida.
   - La primera vez que ejecutas una prueba, fallará. ¡Esto es intencional! Debes abrir el archivo del ejercicio y escribir el código necesario para que la prueba pase.
4. Algunos ejercicios tienen condiciones de prueba definidas en su archivo spec como `test.skip` en comparación con `test`. Esto es intencional. Después de pasar una `test`, cambiarás la siguiente `test.skip` a `test` y probarás tu código nuevamente. Harás esto hasta que todas las condiciones estén satisfechas. **Todas las pruebas deben pasar al mismo tiempo**, y no deberías tener ninguna instancia de `test.skip` cuando termines un ejercicio.
5. Una vez que completes con éxito un ejercicio, consulta la rama `solutions` de TOP para compararla con la tuya.
   - ¡No debes consultar la solución de un ejercicio hasta que lo hayas completado!
   - Ten en cuenta que la solución de TOP no es la única solución. En general, siempre y cuando todas las pruebas pasen, tu solución debería estar bien.
6. No envíes tus soluciones a este repositorio, ya que cualquier solicitud de extracción que lo haga se cerrará sin fusionar.

**Nota**: Debido a la forma en que Jest maneja las pruebas fallidas, puede devolver un código de salida de 1 si alguna prueba falla. NPM interpretará esto como un error y es posible que veas algunos mensajes de `npm ERR!` después de que Jest se ejecute. Puedes ignorar estos mensajes o ejecutar tu prueba con `npm test exerciseName.spec.js --silent` para suprimir los errores.

El primer ejercicio, `helloWorld`, te guiará a través del proceso en detalle.

## Depuración

Para depurar funciones, puedes ejecutar las pruebas en la terminal de depuración de Visual Studio Code. Puedes abrir esto haciendo clic en el icono "Run and Debug" a la izquierda o presionando `ctrl + shift + D`, luego haciendo clic en JavaScript Debug Terminal. Podrás establecer puntos de interrupción como lo harías en el depurador de Chrome DevTools. Puedes ejecutar `npm test exerciseName.spec.js` para ejecutar tu código hasta el punto de interrupción y seguir tu código según sea necesario. **NOTA**: Para aprovechar el depurador, **DEBES** ejecutar el script en la terminal de depuración, no en la terminal de bash o zsh.
